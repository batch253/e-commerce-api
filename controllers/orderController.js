const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

module.exports.createOrder = async (data) => {

	Product.findById({productId: data.productId})
		let newOrder = new Order({
			userId : data.userId,
			products: {
				productId: data.productId,
				quantity: data.quantity,
				image: data.image
			},
			totalAmount: data.quantity*data.price
		});
		return newOrder.save().then(result => {
			if (result) {
				return true
			} else {
				return false
			}
		}).catch(err=> err)
};

module.exports.getCart = (reqParams) => {
	return Order.find({userId:reqParams.userId}).then(result => result)
		.catch(err => err);
};