
const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");
const Product = require("../models/Product");
const auth = require("../auth");

router.post("/createOrder", (req, res) => {
	let data = {
		userId: req.body.userId,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity,
		image: req.body.image,
		totalAmount: req.body.totalAmount
	};

	orderController.createOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

});

router.get("/cart/:userId", (req, res) => {
	orderController.getCart(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

module.exports = router;
