const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

// Route for retrieving user details
router.get("/details", (req, res) => {

	// Provides the user's ID for the getProfile controller method
	/*userController.getProfile({ userId : req.body.id }).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));*/

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({ userId : userData.id }).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


module.exports = router;
