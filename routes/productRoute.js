const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

router.post("/addProduct", (req, res) => {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.get("/all", (req, res) => {
		productController.getAllProduct().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

});

router.get("/allactive", (req, res) => {
		productController.getAllActive().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.get("/:productId", (req, res) => {
		productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});


router.put("/:productId/",  (req, res) => {

		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.put("/:productId/archive", (req, res) => {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

router.put("/:productId/activate", (req, res) => {
		productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

});



module.exports = router;